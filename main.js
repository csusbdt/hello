var http = require('http');

console.log(process.env.PORT);

var server = http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.end('Hello.\n');
});

if (process.env.PORT === undefined) {
  console.log('Running locally; browse to http://localhost:8888/\n');
  server.listen(8888, '127.0.0.1');
} else { // Running on c9.io.
  server.listen(process.env.PORT, '0.0.0.0');
}